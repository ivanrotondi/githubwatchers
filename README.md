# README #

L'app si basa sull'architettura M-V-VM con l'ausilio di RXJava per la comunicazione repository/ViewModel e LiveData per la comunicazione ViewModel/View.

Il rest client è stato realizzato mediante la libreria FastAndroidNetworking e comunica i dati alle repository mediante RxObservable

Le due view dell'app contengono entrambe due EndlessRecyclerView ed ambedue le repository da cui ricevono i dati sono predisposte per fetchare i dati e ripristinarli in caso di OnDestroy causato dal sistema.

Le business logics sono state centralizzate il più possibile per aiutare la mantenibilità del codice e sfruttare il riutilizzo del codice.
Si avranno così delle base repository e delle base viewmodel in cui vi saranno le logiche che gestiscono l'endless scoll.

L'app cerca di seguire le indicazioni riportate dalla documentazione di GitHub per gestione della paginazione e dei possibili errori.