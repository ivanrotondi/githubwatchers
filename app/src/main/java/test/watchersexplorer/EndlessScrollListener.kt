package test.watchersexplorer

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import lib.baseuiandlogic.utils.livedata.LibSingleLiveEvent

class EndlessScrollListener(val layoutManager: LinearLayoutManager, private val threshold: Int): RecyclerView.OnScrollListener() {

    private var firstVisibleInListview: Int = 0
    var thresholdLiveData = LibSingleLiveEvent<Int>()

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val totalItemCount = layoutManager.itemCount
        val itemVisible = layoutManager.findFirstVisibleItemPosition()

        if (itemVisible > totalItemCount.div(threshold)
            && itemVisible > firstVisibleInListview) {
            thresholdLiveData.value = totalItemCount
        }
        firstVisibleInListview = itemVisible
    }
}