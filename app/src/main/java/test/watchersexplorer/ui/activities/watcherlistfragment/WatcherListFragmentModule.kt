package test.watchersexplorer.ui.activities.watcherlistfragment

import android.arch.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import lib.baseuiandlogic.utils.dagger.ViewModelProviderFactory
import test.watchersexplorer.repositories.RepoWatchersRepository

@Module
class WatcherListFragmentModule {

    @Provides
    internal fun listWatcherFragmentViewModelProvider(listWatcherFragment: WatcherListFragmentViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(listWatcherFragment)
    }

    @Provides
    internal fun provideWatcherListFragmentViewModel(repoWatchersRepository: RepoWatchersRepository): WatcherListFragmentViewModel {
        return WatcherListFragmentViewModel(repoWatchersRepository)
    }
}