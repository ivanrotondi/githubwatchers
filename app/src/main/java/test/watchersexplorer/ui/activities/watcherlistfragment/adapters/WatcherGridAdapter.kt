package test.watchersexplorer.ui.activities.watcherlistfragment.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_grid_watcher.view.*
import test.watchersexplorer.R
import test.watchersexplorer.base.BaseRecyclerAdapter
import test.watchersexplorer.data.models.response.Watcher

class WatcherGridAdapter(val requestManager: RequestManager): BaseRecyclerAdapter<WatcherItemViewHolder, Watcher>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WatcherItemViewHolder {
        return WatcherItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_grid_watcher, parent, false))
    }

    override fun onBindViewHolder(holder: WatcherItemViewHolder, position: Int) {
        holder.watcher = items[position]
        requestManager.clear(holder.view.iv_grid_element)
        requestManager.asBitmap().apply(RequestOptions().override(100, 100)).load(items[position].avatarUrl).into(holder.view.iv_grid_element)
    }
}