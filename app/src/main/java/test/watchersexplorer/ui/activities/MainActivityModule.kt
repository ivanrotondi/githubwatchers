package test.watchersexplorer.ui.activities

import android.arch.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import lib.baseuiandlogic.utils.dagger.ViewModelProviderFactory

@Module
class MainActivityModule {
    @Provides
    internal fun mainViewModelProvider(mainActivityViewModel: MainActivityViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(mainActivityViewModel)
    }

    @Provides
    internal fun provideMainViewModel(): MainActivityViewModel {
        return MainActivityViewModel()
    }
}