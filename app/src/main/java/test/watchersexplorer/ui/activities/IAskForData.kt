package test.watchersexplorer.ui.activities

interface IAskForData {
    fun ask()
}