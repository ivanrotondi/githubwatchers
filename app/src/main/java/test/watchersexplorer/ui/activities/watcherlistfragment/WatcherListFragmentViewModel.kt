package test.watchersexplorer.ui.activities.watcherlistfragment

import test.watchersexplorer.base.BaseViewModelWithPaging
import test.watchersexplorer.data.models.request.GetListWatcherRequest
import test.watchersexplorer.data.models.response.Watcher
import test.watchersexplorer.data.models.response.WatcherListResponse
import test.watchersexplorer.repositories.RepoWatchersRepository
import javax.inject.Inject

class WatcherListFragmentViewModel @Inject constructor(repoWatchersRepository: RepoWatchersRepository):
    BaseViewModelWithPaging<GetListWatcherRequest, WatcherListResponse, RepoWatchersRepository, Watcher>(repoWatchersRepository) {

    fun buildRequest(owner: String, repoName: String) {
        request = GetListWatcherRequest(owner, repoName)
    }
}