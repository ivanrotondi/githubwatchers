package test.watchersexplorer.ui.activities.searchrepofragment.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import test.watchersexplorer.base.BaseRecyclerAdapter
import test.watchersexplorer.R
import test.watchersexplorer.data.models.response.SearchItems
import test.watchersexplorer.customviews.transition.SingleTransition

class RepoListAdapter(val onCLick: (SearchItems, position: Int, view: View) -> Unit): BaseRecyclerAdapter<RepoItemViewHolder, SearchItems>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoItemViewHolder {
        return RepoItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_repository, parent, false))
    }

    override fun onBindViewHolder(holder: RepoItemViewHolder, position: Int) {
        holder.repo = items[position]
        holder.view.transitionName = SingleTransition.TRANSITION_NAME_SUFFIX + position
        holder.view.setOnClickListener{ onCLick(items[position], position, holder.view) }
    }

}