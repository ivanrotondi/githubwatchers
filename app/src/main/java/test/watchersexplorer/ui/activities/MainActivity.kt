package test.watchersexplorer.ui.activities

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import test.watchersexplorer.R
import test.watchersexplorer.base.WEBaseActivity
import test.watchersexplorer.ui.activities.searchrepofragment.SearchReposFragment
import javax.inject.Inject

class MainActivity : WEBaseActivity<MainActivityViewModel>() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var mainActivityViewModel: MainActivityViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            changeFragmentWithoutHistory(
                R.id.fl_main_activity,
                SearchReposFragment(),
                SearchReposFragment.SEARCH_REPOS_TAG
            )
        }

        setSupportActionBar(findViewById(R.id.lo_toolbar))
    }

    override fun getViewModel(): MainActivityViewModel {
        mainActivityViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainActivityViewModel::class.java)
        return mainActivityViewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }
}
