package test.watchersexplorer.ui.activities.searchrepofragment.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.item_repository.view.*
import test.watchersexplorer.R
import test.watchersexplorer.data.models.response.SearchItems
import test.watchersexplorer.utlis.MethodUtils

class RepoItemViewHolder(val view: View): RecyclerView.ViewHolder(view) {

    var repo: SearchItems? = null
    set(value) {
        field = value
        view.tv_repo_name.text = value?.name
        view.tv_repo_description.text = value?.description
        view.tv_repo_language.text = value?.language
        view.tv_update_date.text = MethodUtils.parsingDate(value?.updatedAt)
        view.tv_stars_forks.text = String.format(view.context.resources.getString(R.string.stars_and_forks), value?.stars, value?.forksCount)
    }
}