package test.watchersexplorer.ui.activities.watcherlistfragment.adapters

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.item_grid_watcher.view.*
import test.watchersexplorer.data.models.response.Watcher

class WatcherItemViewHolder(val view: View): RecyclerView.ViewHolder(view) {

    var watcher: Watcher? = null
    set(value) {
        field = value
        view.tv_grid_element.text = value?.login
    }
}