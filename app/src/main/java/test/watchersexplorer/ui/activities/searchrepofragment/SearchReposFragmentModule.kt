package test.watchersexplorer.ui.activities.searchrepofragment

import android.arch.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import lib.baseuiandlogic.utils.dagger.ViewModelProviderFactory
import test.watchersexplorer.repositories.SearchReposRepository

@Module
class SearchReposFragmentModule {

    @Provides
    internal fun searchFragmentViewModelProvider(searchReposFragmentViewModel: SearchReposFragmentViewModel): ViewModelProvider.Factory {
        return ViewModelProviderFactory(searchReposFragmentViewModel)
    }

    @Provides
    internal fun provideSearchtRepoFragmentViewModel(searchReposRepository: SearchReposRepository): SearchReposFragmentViewModel {
        return SearchReposFragmentViewModel(searchReposRepository)
    }

}