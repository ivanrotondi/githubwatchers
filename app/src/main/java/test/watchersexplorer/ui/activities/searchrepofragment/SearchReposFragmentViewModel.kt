package test.watchersexplorer.ui.activities.searchrepofragment

import test.watchersexplorer.base.BaseViewModelWithPaging
import test.watchersexplorer.data.ApiParamsAndPaths
import test.watchersexplorer.data.models.request.GetSearchRepositoriesRequest
import test.watchersexplorer.data.models.response.SearchItems
import test.watchersexplorer.data.models.response.SearchRepositoriesResponse
import test.watchersexplorer.repositories.SearchReposRepository
import javax.inject.Inject

class SearchReposFragmentViewModel @Inject constructor(searchReposRepository: SearchReposRepository):
    BaseViewModelWithPaging<GetSearchRepositoriesRequest, SearchRepositoriesResponse, SearchReposRepository, SearchItems>(searchReposRepository) {

    fun isSearchExecuted(): Boolean {
        return !request?.param.isNullOrEmpty()
    }

    fun getSearchedRequest(): GetSearchRepositoriesRequest? {
        return request
    }

    fun buildRequest(param: HashMap<String, String>, sort: String = "", order: String = "") {
        request = GetSearchRepositoriesRequest()

        var requestParam = param[ApiParamsAndPaths.SEARCH_REPOSITORIES_PARAM_PARAM]
        param.remove(ApiParamsAndPaths.SEARCH_REPOSITORIES_PARAM_PARAM)

        for ((key, value) in param) {
            if (!value.isEmpty()) {
                when (key) {
                    ApiParamsAndPaths.SEARCH_REPOSITORIES_LANGUAGE_PARAM -> {
                        request!!.language = value
                    }
                }
                requestParam += "+$key:$value"
            }
        }

        request!!.param = requestParam!!
        request!!.order = order
        request!!.sort = sort
    }
}