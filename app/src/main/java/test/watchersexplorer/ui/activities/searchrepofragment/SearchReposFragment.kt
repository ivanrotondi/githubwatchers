package test.watchersexplorer.ui.activities.searchrepofragment

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.fragment_search_repositories.*
import kotlinx.android.synthetic.main.widget_search_view.*
import test.watchersexplorer.R
import test.watchersexplorer.base.WEBaseActivity
import test.watchersexplorer.base.WEBaseFragmentWithPagination
import test.watchersexplorer.data.ApiParamsAndPaths
import test.watchersexplorer.data.models.request.GetSearchRepositoriesRequest
import test.watchersexplorer.data.models.response.BaseResponse
import test.watchersexplorer.data.models.response.SearchItems
import test.watchersexplorer.dialogs.DialogFilter
import test.watchersexplorer.dialogs.DialogFilterCallback
import test.watchersexplorer.errors.ErrorManager
import test.watchersexplorer.repositories.RepoWatchersRepository
import test.watchersexplorer.ui.activities.searchrepofragment.adapters.RepoListAdapter
import test.watchersexplorer.ui.activities.watcherlistfragment.WatcherListFragment
import test.watchersexplorer.EndlessScrollListener
import test.watchersexplorer.customviews.transition.SingleTransition
import test.watchersexplorer.customviews.transition.TransitionListToDetail
import javax.inject.Inject

class SearchReposFragment: WEBaseFragmentWithPagination<SearchReposFragmentViewModel, SearchItems>(), View.OnClickListener, DialogFilterCallback {
    companion object {
        const val SEARCH_REPOS_TAG = "search_repo_tag"
        const val FILTER_ACTIVE = "filter_active"
        const val SEARCH_EXECUTED = "search_executed"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var searchReposFragmentViewModel: SearchReposFragmentViewModel

    private lateinit var dialogFilter: DialogFilter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = searchReposFragmentViewModel

        dialogFilter = DialogFilter(context!!, this)

        adapter = RepoListAdapter { searchItems: SearchItems, position: Int, view: View ->
            val bundle = Bundle()
            bundle.putString(SingleTransition.BUNDLE_TRANSITION_KEY, SingleTransition.TRANSITION_NAME_SUFFIX + position)
            bundle.putParcelable(WatcherListFragment.ITEM_SELECTED, searchItems)

            val watcherFragment = WatcherListFragment()
            watcherFragment.arguments = bundle

            (activity as WEBaseActivity<*>).changeFragmentWithSingleTransition(this,
                watcherFragment,
                WatcherListFragment.LIST_WATCHERS_TAG,
                TransitionListToDetail(view, SingleTransition.TRANSITION_NAME_SUFFIX + position))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as WEBaseActivity<*>).hideToolBar()

        iv_filter.setOnClickListener(this)
        btn_search.setOnClickListener(this)

        et_search_repo.setButton(btn_search)

        val linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rv_search_recycler.layoutManager = linearLayoutManager
        rv_search_recycler.adapter = adapter

        scrollListener = EndlessScrollListener(linearLayoutManager, 2)
        attachScrollObserver()
        rv_search_recycler.addOnScrollListener(scrollListener)


        if (dialogFilter.active) {
            filterActive()
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_search -> {
                hideKeyboard()

                if (!ErrorManager.isCallExceeded(RepoWatchersRepository::class.java.name)) {
                    val list = HashMap<String, String>()
                    list[ApiParamsAndPaths.SEARCH_REPOSITORIES_PARAM_PARAM] = et_search_repo.text.toString()
                    list[ApiParamsAndPaths.SEARCH_REPOSITORIES_LANGUAGE_PARAM] = dialogFilter.language

                    tv_no_repo_found.visibility = View.GONE
                    adapter.initAdapter()
                    attachScrollObserver()
                    searchReposFragmentViewModel.buildRequest(list, dialogFilter.sort, dialogFilter.order)
                    searchReposFragmentViewModel.getFirstPageOfPagination()
                }
                else {
                    (activity as WEBaseActivity<*>).showErrorDialog(BaseResponse(getString(R.string.error_rate_limit_exceeded)))
                }
            }

            R.id.iv_filter -> {
                dialogFilter.show()
            }
        }
    }

    override fun filterActive() {
        iv_filter.setImageResource(R.drawable.ic_filter_red)
    }

    override fun filterInactive() {
        iv_filter.setImageResource(R.drawable.ic_filter_gray)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState?.getParcelable<GetSearchRepositoriesRequest>(SEARCH_EXECUTED) != null) {
            searchReposFragmentViewModel.request = savedInstanceState.getParcelable(SEARCH_EXECUTED)

            dialogFilter.active = savedInstanceState.getBoolean(FILTER_ACTIVE)
            if (dialogFilter.active) {
                filterActive()
                dialogFilter.language = searchReposFragmentViewModel.request?.language!!
                dialogFilter.order = searchReposFragmentViewModel.request?.order!!
                dialogFilter.sort = searchReposFragmentViewModel.request?.sort!!
            }
            else {
                filterInactive()
            }

            searchReposFragmentViewModel.tryToGetSavedList()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (searchReposFragmentViewModel.isSearchExecuted()) {
            outState.putParcelable(SEARCH_EXECUTED, searchReposFragmentViewModel.getSearchedRequest())
            outState.putBoolean(FILTER_ACTIVE, dialogFilter.active)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (dialogFilter.isShowing) dialogFilter.dismiss()
    }

    override fun getViewModel(): SearchReposFragmentViewModel {
        searchReposFragmentViewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(SearchReposFragmentViewModel::class.java)
        return searchReposFragmentViewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_search_repositories
    }

    override fun noDataFound() {
        tv_no_repo_found.visibility = View.VISIBLE
    }
}