package test.watchersexplorer.ui.activities.watcherlistfragment

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_watcher_list.*
import test.watchersexplorer.R
import test.watchersexplorer.base.WEBaseActivity
import test.watchersexplorer.base.WEBaseFragmentWithPagination
import test.watchersexplorer.data.models.response.SearchItems
import test.watchersexplorer.data.models.response.Watcher
import test.watchersexplorer.ui.activities.IAskForData
import test.watchersexplorer.ui.activities.searchrepofragment.adapters.RepoItemViewHolder
import test.watchersexplorer.ui.activities.watcherlistfragment.adapters.WatcherGridAdapter
import test.watchersexplorer.EndlessScrollListener
import test.watchersexplorer.customviews.transition.SingleTransition
import javax.inject.Inject

class WatcherListFragment: WEBaseFragmentWithPagination<WatcherListFragmentViewModel, Watcher>() {
    companion object {
        const val LIST_WATCHERS_TAG = "list_watcher_tag"
        const val ITEM_SELECTED = "item_selected"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val fromScratch: IAskForData = object :
        IAskForData {
        override fun ask() {
            viewModel.buildRequest(itemSelected.owner?.login!!, itemSelected.name!!)
            viewModel.getFirstPageOfPagination()
        }
    }

    private val fromSaved = object : IAskForData {
        override fun ask() {
            viewModel.buildRequest(itemSelected.owner?.login!!, itemSelected.name!!)
            viewModel.tryToGetSavedList()
        }
    }

    private var askDataObject: IAskForData = fromScratch

    private lateinit var itemSelected: SearchItems


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val viewParent = super.onCreateView(inflater, container, savedInstanceState)

        val bundle = arguments
        if (bundle != null) {
            itemSelected = bundle.getParcelable(ITEM_SELECTED)!!

            viewParent!!.findViewById<ConstraintLayout>(R.id.lo_header).transitionName = bundle.getString(SingleTransition.BUNDLE_TRANSITION_KEY)

            val repoItem = RepoItemViewHolder(viewParent)
            repoItem.repo = itemSelected

            (activity as WEBaseActivity<*>).showToolBarWithBackButton(itemSelected.fullName)
        }

        return viewParent
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val gridLayoutManager = GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, true)

        adapter = WatcherGridAdapter(Glide.with(context!!))

        rv_grid_list.layoutManager = gridLayoutManager
        rv_grid_list.adapter = adapter
        gridLayoutManager.reverseLayout = false

        scrollListener = EndlessScrollListener(gridLayoutManager, 2)
        attachScrollObserver()

        rv_grid_list.addOnScrollListener(scrollListener)
    }

    override fun onStart() {
        super.onStart()
        askDataObject.ask()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState != null) askDataObject = fromSaved
    }

    override fun getViewModel(): WatcherListFragmentViewModel {
        viewModel = ViewModelProviders.of(activity!!, viewModelFactory).get(WatcherListFragmentViewModel::class.java)
        return viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_watcher_list
    }

    override fun noDataFound() {
        tv_no_watchers_found.visibility = View.VISIBLE
    }
}