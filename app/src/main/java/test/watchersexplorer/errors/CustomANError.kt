package test.watchersexplorer.errors

import com.androidnetworking.error.ANError

class CustomANError(var messageToShow: String? = null, var error: ANError? = null)