package test.watchersexplorer.errors

import android.annotation.SuppressLint
import android.content.Context
import com.androidnetworking.error.ANError
import test.watchersexplorer.R
import test.watchersexplorer.data.ApiParamsAndPaths
import java.util.*
import javax.inject.Singleton

@SuppressLint("StaticFieldLeak")
@Singleton
object ErrorManager {

    var appContext: Context? = null

    private val repoMapError: ArrayList<String> = ArrayList()

    fun addRepoInExceededList(repo: String) {
        repoMapError.add(repo)
    }

    fun isCallExceeded(repo: String): Boolean {
        return repoMapError.contains(repo)
    }

    fun removeRepoFromExceededList(repo: String) {
        if (repoMapError.contains(repo)) repoMapError.remove(repo)
    }

    fun provideErrorMessage(error: ANError): String {
        return when (error.errorCode) {
            ApiParamsAndPaths.RATE_LIMIT_EXCEEDED_ERROR_NUMBER -> {
                appContext!!.resources.getString(R.string.error_rate_limit_exceeded)
            }

            ApiParamsAndPaths.RESULT_1000_REACHED -> {
                appContext!!.resources.getString(R.string.error_reach_1000)
            }

            ApiParamsAndPaths.TIMEOUT_FROM_SERVER_ERROR -> {
                appContext!!.resources.getString(R.string.error_timeout_from_server)
            }

            else -> {
                error.errorDetail
            }
        }
    }

    fun addExceededServiceCall(errorCode: Int, clazz: String) {
        when (errorCode) {
            ApiParamsAndPaths.RATE_LIMIT_EXCEEDED_ERROR_NUMBER -> {
                addRepoInExceededList(
                    clazz
                )
            }
        }
    }
}