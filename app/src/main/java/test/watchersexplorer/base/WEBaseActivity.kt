package test.watchersexplorer.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import lib.baseuiandlogic.logic.LibBaseViewModel
import lib.baseuiandlogic.ui.LibBaseActivity
import test.watchersexplorer.R
import test.watchersexplorer.data.models.response.BaseResponse
import test.watchersexplorer.dialogs.DialogLoader
import test.watchersexplorer.customviews.transition.SingleTransition

abstract class WEBaseActivity<V : LibBaseViewModel>: LibBaseActivity<V>() {

    private lateinit var dialogLoader: AlertDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialogLoader = DialogLoader(this)
    }

    fun showOrHideLoader(it: Boolean) {
        if (it) {
            dialogLoader.show()
        }
        else {
            dialogLoader.dismiss()
        }
    }

    fun changeFragmentWithSingleTransition(currentFragment: Fragment,
                                           nextFragment: Fragment,
                                           nextFragmentTag: String,
                                           transition: SingleTransition) {

        val tInflater = android.support.transition.TransitionInflater.from(this)
        currentFragment.sharedElementReturnTransition = tInflater.inflateTransition(transition.sharedElementReturnTransition)
        currentFragment.exitTransition = tInflater.inflateTransition(transition.exitTransition)

        nextFragment.sharedElementEnterTransition = tInflater.inflateTransition(transition.sharedElementEnterTransition)
        nextFragment.enterTransition = tInflater.inflateTransition(transition.enterTransition)

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fl_main_activity, nextFragment, nextFragmentTag)
        fragmentTransaction.addToBackStack(nextFragmentTag)
        fragmentTransaction.addSharedElement(transition.sharedView, transition.sharedElementName)
        fragmentTransaction.commit()
    }

    fun showToolBarWithBackButton(title: String)  {
        supportActionBar?.title = title
        showToolBarWithBackButton()
    }

    fun showToolBarWithBackButton() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        showToolBar()
    }

    fun showErrorDialog(it: BaseResponse) {
        val errorDialog = AlertDialog.Builder(this)
        errorDialog.setTitle(getString(R.string.dialog_error_title))
        errorDialog.setMessage(it.error)
        errorDialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (dialogLoader.isShowing) showOrHideLoader(false)
    }

    override fun onFragmentAttached() {

    }

    override fun onFragmentDetached(p0: String?) {

    }
}