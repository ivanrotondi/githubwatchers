package test.watchersexplorer.base;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import lib.baseuiandlogic.ui.LibBaseFragment;
import lib.baseuiandlogic.utils.NetworkUtils;
import test.watchersexplorer.data.ApiParamsAndPaths;
import test.watchersexplorer.data.models.response.BaseResponse;
import test.watchersexplorer.data.models.response.PagingResponse;
import test.watchersexplorer.EndlessScrollListener;

import java.util.Objects;

abstract public class WEBaseFragmentWithPagination<V extends BaseViewModelWithPaging, ITEM> extends LibBaseFragment<V> {

    public V viewModel;

    public BaseRecyclerAdapter adapter;
    public EndlessScrollListener scrollListener;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel.getShowLoaderLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean it) {
                if (it != null) {
                    ((WEBaseActivity) Objects.requireNonNull(getActivity())).showOrHideLoader(it);
                }
            }
        });

        viewModel.getResponseLiveData().observe(this, new Observer<PagingResponse<ITEM>>() {
            @Override
            public void onChanged(@Nullable PagingResponse<ITEM> itemPagingResponse) {
                if (itemPagingResponse.getList().size() == 0 && adapter.getItems().isEmpty()) {
                    noDataFound();
                }
                else {
                    adapter.updateAdapter(itemPagingResponse.getList());
                }
            }
        });

        viewModel.getErrorLiveData().observe(this, new Observer<BaseResponse>() {
            @Override
            public void onChanged(@Nullable BaseResponse baseResponse) {
                ((WEBaseActivity) Objects.requireNonNull(getActivity())).showErrorDialog(baseResponse);
                // if 1000 result loaded from service, avoid to call the service again detaching LiveData in scoller
                if (baseResponse.getErrorCode() == ApiParamsAndPaths.RESULT_1000_REACHED) {
                    scrollListener.getThresholdLiveData().removeObservers(getViewLifecycleOwner());
                }
            }
        });
    }

    public void attachScrollObserver() {
        scrollListener.getThresholdLiveData().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (NetworkUtils.isNetworkConnected(Objects.requireNonNull(getContext()))) {
                    viewModel.getNextPage();
                }
            }
        });
    }

    public abstract void noDataFound();
}
