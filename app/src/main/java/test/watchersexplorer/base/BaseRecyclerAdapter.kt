package test.watchersexplorer.base

import android.support.v7.widget.RecyclerView
import java.util.*

abstract class BaseRecyclerAdapter<V: RecyclerView.ViewHolder, I>: RecyclerView.Adapter<V>() {

    var items: LinkedList<I> = LinkedList()


    override fun getItemCount(): Int {
        return items.size
    }

    fun initAdapter() {
        this.items.clear()
        notifyDataSetChanged()
    }

    fun updateAdapter(items: LinkedList<I>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun initAndUpdateAdapter(items: LinkedList<I>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }
}