package test.watchersexplorer.base

import android.util.Log
import com.androidnetworking.error.ANError
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import lib.baseuiandlogic.logic.LibBaseRepository
import lib.baseuiandlogic.utils.livedata.LibSingleLiveEvent
import lib.baseuiandlogic.utils.rx.LibSchedulerProvider
import test.watchersexplorer.data.models.response.PagingResponse
import test.watchersexplorer.data.parsers.IHeaderParser
import test.watchersexplorer.errors.CustomANError
import test.watchersexplorer.errors.ErrorManager

open abstract class BasePagingRepository<REQ, RES : PagingResponse<ITEM>, ITEM>(schedulerProvider: LibSchedulerProvider, private val errorManager: ErrorManager, private val parser: IHeaderParser<REQ, RES>): LibBaseRepository<REQ, RES>(schedulerProvider) {
    var request: REQ? = null
    var response: RES? = null

    var obs: Observable<RES>? = null
    var observableDisposable: Disposable? = null

    var callRunning = false
    var isFirstCall = true

    var showLoaderLiveData: LibSingleLiveEvent<Boolean> = LibSingleLiveEvent()

    /**
     * Every inherit class specify which API should call
     */
    abstract fun fillObservable(): Observable<RES>

    abstract fun createAndDeliverErrorResult(error: CustomANError)

    /**
     * Parse the response header to build the new request
     */
    private fun getNextRequestFromHeader(response: RES, oldRequest: REQ): REQ? {
        return parser.parse(response, oldRequest)
    }

    init {
        publishSubject = PublishSubject.create()
    }

    fun startPagination(request: REQ) {
        cancelCallAndInitRepository()

        isFirstCall = true
        this.request = request
        execute(request)
    }

    override fun execute(request: REQ?): Observable<RES> {
        if (canExecuteCall()) {

            if (isFirstCall) showLoaderLiveData.value = true

            callInExecution()

            obs = fillObservable()

            observableDisposable = obs!!.subscribe(
                { result ->
                    handleSuccessResponse(result)
                },
                { e ->
                    handleErrorResponse(e as ANError)
                }
            )
        }

        return publishSubject
    }

    fun handleSuccessResponse(result: RES) {
        //Log.d("DEBUG", "Success API call")
        if (isResponseComplete(result)){
            if (result.getList() != null) {

                this.request = getNextRequestFromHeader(result, this.request!!)

                // first call executed: remove loader and save result
                if (response == null) {
                    isFirstCall = false
                    response = result
                    showLoaderLiveData.value = false
          //          Log.d("DEBUG", "First call done")
                }
                // isn't the first call, add result in existing response
                else {
                    response?.addToList(result.getList()!!)
          //          Log.d("DEBUG", "getting more data!")
                }
            }

            errorManager.removeRepoFromExceededList(javaClass.name)
            deliverData(result)
        }
        else {
            isFirstCall = false
            showLoaderLiveData.value = false
            val error = ANError()
            error.errorCode = 504
            handleErrorResponse(error)
        }
        callRunning = false
    }

    fun handleErrorResponse(error: ANError) {
        if (isFirstCall) showLoaderLiveData.value = false
        isFirstCall = false

        val customError = CustomANError()
        customError.error = error
        customError.messageToShow = errorManager.provideErrorMessage(error)

        observableDisposable?.dispose()
        obs = null
        //Log.d("DEBUG", "ERROR: " + error.errorBody)

        callRunning = false
        // if call is already exceeded do not deliver error to avoid spamming AlertDialogs
        if (!errorManager.isCallExceeded(javaClass.name)) {
            createAndDeliverErrorResult(customError)
            // if errorCode is 403, add to exceeded rate limit list
            errorManager.addExceededServiceCall(error.errorCode, javaClass.name)
        }
    }

    fun executeOrGetSavedList(request: REQ) {
        if (response?.getList() != null && response?.getList()!!.size != 0) {
            publishSubject.onNext(response!!)
        }
        else {
            startPagination(request)
        }
    }

    fun cancelCallAndInitRepository() {
        this.request = null
        response = null
        cancelCall()
    }

    fun cancelCall() {
        observableDisposable?.dispose()
    }

    /**
     * Check if the API call is already exceeded rate limit, and if another call is running
     */
    private fun canExecuteCall(): Boolean {
        return this.request != null && !callRunning
    }

    fun callInExecution() {
        callRunning = true
    }

    fun deliverData(data: RES) {
        publishSubject.onNext(data)
    }

    abstract fun isResponseComplete(result: RES): Boolean
}