package test.watchersexplorer.base

import android.arch.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import lib.baseuiandlogic.logic.LibBaseViewModel
import lib.baseuiandlogic.utils.livedata.LibSingleLiveEvent
import test.watchersexplorer.data.models.response.PagingResponse

abstract class BaseViewModelWithPaging<REQ, RES : PagingResponse<ITEM>, REPO : BasePagingRepository<REQ, RES, ITEM>, ITEM>(
    repository: REPO): LibBaseViewModel() {

    private var pagingDisposable: Disposable? = null
    private var pagingObservable: Observable<RES>? = null

    var repositoryWithPaging: REPO = repository
    var request: REQ? = null

    val responseLiveData: MutableLiveData<RES> = LibSingleLiveEvent()
    val errorLiveData: MutableLiveData<RES> = LibSingleLiveEvent()
    var showLoaderLiveData: MutableLiveData<Boolean> = repository.showLoaderLiveData


    fun getFirstPageOfPagination() {
        subscribeObservable()
        repositoryWithPaging.startPagination(request!!)
    }

    fun getNextPage() {
        repositoryWithPaging.execute(null)
    }

    /**
     * Called if app was destroyed by the system and recreated later
     */
    fun tryToGetSavedList() {
        subscribeObservable()
        repositoryWithPaging.executeOrGetSavedList(request!!)
    }

    private fun subscribeObservable() {
        if (pagingObservable == null) {
            // get the observable before subscribing
            pagingObservable = repositoryWithPaging.publishSubject
            // subscribe observable, on result emit relative LiveData
            pagingDisposable = pagingObservable!!.subscribe { result ->

                if (result?.getErrorString().isNullOrEmpty()) {
                    if (result?.getList() != null) {
                        responseLiveData.value = result
                    }
                }
                else {
                    errorLiveData.value = result
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        pagingDisposable?.dispose()
        pagingObservable = null
    }
}