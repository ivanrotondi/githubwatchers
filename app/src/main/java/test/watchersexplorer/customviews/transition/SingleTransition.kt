package test.watchersexplorer.customviews.transition

import android.view.View

open class SingleTransition(val sharedView: View, val sharedElementName: String) {
    companion object {
        const val TRANSITION_NAME_SUFFIX = "transition"
        const val BUNDLE_TRANSITION_KEY = "transitionName"
    }

    var sharedElementReturnTransition: Int = 0
    var exitTransition: Int = 0
    var sharedElementEnterTransition: Int = 0
    var enterTransition: Int = 0
}