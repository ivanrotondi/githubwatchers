package test.watchersexplorer.customviews.transition

import android.view.View
import test.watchersexplorer.R

class TransitionListToDetail(sharedView: View, sharedElementName: String): SingleTransition(sharedView, sharedElementName) {
    init {
        sharedElementReturnTransition = R.transition.default_transition
        exitTransition = android.R.transition.no_transition
        sharedElementEnterTransition = R.transition.default_transition
        enterTransition = R.transition.default_transition
    }
}