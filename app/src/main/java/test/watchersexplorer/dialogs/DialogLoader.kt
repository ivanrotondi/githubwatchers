package test.watchersexplorer.dialogs

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.lib_dialog_loader.*
import test.watchersexplorer.R

class DialogLoader(context: Context): AlertDialog(context, android.R.style.Theme_Translucent_NoTitleBar) {

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lib_dialog_loader)
        setCancelable(false)
        Glide.with(context)
            .asGif()
            .load(R.drawable.balls_loader)
            .into(loader_gif)
    }
}