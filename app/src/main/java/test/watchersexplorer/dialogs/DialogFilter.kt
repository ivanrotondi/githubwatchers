package test.watchersexplorer.dialogs

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.view.WindowManager
import android.widget.RadioGroup
import kotlinx.android.synthetic.main.dialog_filter.*
import test.watchersexplorer.R
import test.watchersexplorer.data.ApiParamsAndPaths

class DialogFilter(context: Context, private val dialogFilterCallback: DialogFilterCallback): AlertDialog(context), View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    var order: String = ""
    var sort: String = ""
    var language: String = ""

    var active = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_filter)
        setCancelable(true)
    }

    override fun onStart() {
        super.onStart()

        tv_clear.setOnClickListener(this)
        tv_confirm.setOnClickListener(this)

        rg_sort.setOnCheckedChangeListener(this)
        rg_order.setOnCheckedChangeListener(this)

        et_filter_language.setText(language)
        when (order) {
            ApiParamsAndPaths.SEARCH_REPOSITORIES_ORDER_ASC -> {
                rb_asc.isChecked = true
            }

            ApiParamsAndPaths.SEARCH_REPOSITORIES_ORDER_DESC -> {
                rb_desc.isChecked = true
            }
        }

        when (sort) {
            ApiParamsAndPaths.SEARCH_REPOSITORIES_SORT_FORKS -> {
                rb_forks.isChecked = true
            }

            ApiParamsAndPaths.SEARCH_REPOSITORIES_SORT_UPDATED -> {
                rb_updated.isChecked = true
            }

            ApiParamsAndPaths.SEARCH_REPOSITORIES_SORT_STARS -> {
                rb_stars.isChecked = true
            }
        }
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.rb_asc -> {
                order = ApiParamsAndPaths.SEARCH_REPOSITORIES_ORDER_ASC
            }

            R.id.rb_desc -> {
                order = ApiParamsAndPaths.SEARCH_REPOSITORIES_ORDER_DESC
            }

            R.id.rb_forks -> {
                sort = ApiParamsAndPaths.SEARCH_REPOSITORIES_SORT_FORKS
            }

            R.id.rb_updated -> {
                sort = ApiParamsAndPaths.SEARCH_REPOSITORIES_SORT_UPDATED
            }

            R.id.rb_stars -> {
                sort = ApiParamsAndPaths.SEARCH_REPOSITORIES_SORT_STARS
            }
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
        window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.tv_clear -> {
                rg_order.clearCheck()
                rg_sort.clearCheck()
                et_filter_language.text.clear()

                order = ""
                sort = ""
                language = ""

                dialogFilterCallback.filterInactive()
                active = false
                dismiss()
            }

            R.id.tv_confirm -> {
                language = et_filter_language.text.toString()
                dialogFilterCallback.filterActive()
                active = true
                dismiss()
            }
        }
    }
}