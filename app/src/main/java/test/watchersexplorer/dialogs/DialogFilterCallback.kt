package test.watchersexplorer.dialogs

interface DialogFilterCallback {
    fun filterActive()
    fun filterInactive()
}