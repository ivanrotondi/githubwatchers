package test.watchersexplorer.utlis

import java.text.SimpleDateFormat

class MethodUtils {
    companion object {
        private val inputFormat = SimpleDateFormat("yyyy-MM-dd")

        @JvmStatic
        fun parsingDate(date: String?): String {
            if (date != null) {
                val convertedCurrentDate = inputFormat.parse(date)
                return inputFormat.format(convertedCurrentDate)
            }
            return ""
        }
    }
}