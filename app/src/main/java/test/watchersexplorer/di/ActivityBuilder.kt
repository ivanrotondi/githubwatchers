package test.watchersexplorer.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import lib.baseuiandlogic.utils.dagger.scopes.ActivityScope
import test.watchersexplorer.ui.activities.MainActivity
import test.watchersexplorer.ui.activities.MainActivityModule
import test.watchersexplorer.ui.activities.searchrepofragment.SearchReposFragment
import test.watchersexplorer.ui.activities.searchrepofragment.SearchReposFragmentModule
import test.watchersexplorer.ui.activities.watcherlistfragment.WatcherListFragment
import test.watchersexplorer.ui.activities.watcherlistfragment.WatcherListFragmentModule

@Module
abstract class ActivityBuilder {
    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [SearchReposFragmentModule::class])
    abstract fun bindSearchReposFragment(): SearchReposFragment

    @ContributesAndroidInjector(modules = [WatcherListFragmentModule::class])
    abstract fun bindWatcherListFragment(): WatcherListFragment
}