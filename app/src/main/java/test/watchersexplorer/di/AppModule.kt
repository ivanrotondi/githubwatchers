package test.watchersexplorer.di

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import lib.baseuiandlogic.utils.dagger.scopes.AppContext
import lib.baseuiandlogic.utils.rx.LibAppSchedulerProvider
import lib.baseuiandlogic.utils.rx.LibSchedulerProvider
import okhttp3.OkHttpClient
import test.watchersexplorer.data.ApiHelper
import test.watchersexplorer.data.ApiParamsAndPaths
import test.watchersexplorer.data.parsers.SearchReposHeaderParser
import test.watchersexplorer.data.parsers.WatchersHeaderParser
import test.watchersexplorer.errors.ErrorManager
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Binds
    @AppContext
    abstract fun provideContext(application: Application): Context

    @Module
    companion object {
        @JvmStatic
        @Provides
        @Singleton
        fun provideSchedulerProvider(): LibSchedulerProvider {
            return LibAppSchedulerProvider()
        }

        @JvmStatic
        @Provides
        fun provideApiHelper(): ApiHelper {
            return ApiHelper()
        }

        @JvmStatic
        @Provides
        @ParserForReposSearch
        fun provideParserForRepoSearch(): SearchReposHeaderParser {
            return SearchReposHeaderParser()
        }

        @JvmStatic
        @Provides
        @ParserForWatchers
        fun provideParserForWatcher(): WatchersHeaderParser {
            return WatchersHeaderParser()
        }


        @JvmStatic
        @Provides
        @Singleton
        fun provideErrorManager(): ErrorManager {
            return ErrorManager
        }

        @JvmStatic
        @Provides
        fun provideHttpClient(): OkHttpClient {
            return OkHttpClient().newBuilder()
                .connectTimeout(ApiParamsAndPaths.TIMEOUT_CONNECTION_IN_SECONDS, TimeUnit.SECONDS)
                .build()
        }
    }
}