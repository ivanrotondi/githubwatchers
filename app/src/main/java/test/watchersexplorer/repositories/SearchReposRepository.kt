package test.watchersexplorer.repositories

import io.reactivex.Observable
import lib.baseuiandlogic.utils.rx.LibSchedulerProvider
import test.watchersexplorer.errors.CustomANError
import test.watchersexplorer.errors.ErrorManager
import test.watchersexplorer.base.BasePagingRepository
import test.watchersexplorer.data.ApiHelper
import test.watchersexplorer.data.models.request.GetSearchRepositoriesRequest
import test.watchersexplorer.data.models.response.SearchItems
import test.watchersexplorer.data.models.response.SearchRepositoriesResponse
import test.watchersexplorer.data.parsers.SearchReposHeaderParser
import test.watchersexplorer.di.ParserForReposSearch
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class SearchReposRepository @Inject constructor(private val apiHelper: ApiHelper, schedulerProvider: LibSchedulerProvider, errorManager: ErrorManager,@ParserForReposSearch parser: SearchReposHeaderParser): BasePagingRepository<GetSearchRepositoriesRequest, SearchRepositoriesResponse, SearchItems>(schedulerProvider, errorManager, parser) {


    override fun fillObservable(): Observable<SearchRepositoriesResponse> {
        return apiHelper.getSearchRepository(this.request!!)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
    }

    override fun createAndDeliverErrorResult(error: CustomANError) {
        deliverData(SearchRepositoriesResponse(error.messageToShow, error.error!!.errorCode))
    }

    override fun isResponseComplete(result: SearchRepositoriesResponse): Boolean {
        return !result.incompleteResults
    }
}