package test.watchersexplorer.repositories

import io.reactivex.Observable
import lib.baseuiandlogic.utils.rx.LibSchedulerProvider
import test.watchersexplorer.base.BasePagingRepository
import test.watchersexplorer.data.ApiHelper
import test.watchersexplorer.data.models.request.GetListWatcherRequest
import test.watchersexplorer.data.models.response.Watcher
import test.watchersexplorer.data.models.response.WatcherListResponse
import test.watchersexplorer.data.parsers.WatchersHeaderParser
import test.watchersexplorer.di.ParserForWatchers
import test.watchersexplorer.errors.CustomANError
import test.watchersexplorer.errors.ErrorManager
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RepoWatchersRepository @Inject constructor(private val apiHelper: ApiHelper, schedulerProvider: LibSchedulerProvider, errorManager: ErrorManager, @ParserForWatchers parser: WatchersHeaderParser): BasePagingRepository<GetListWatcherRequest, WatcherListResponse, Watcher>(schedulerProvider, errorManager, parser) {

    override fun fillObservable(): Observable<WatcherListResponse> {
        return apiHelper.getWatcherList(this.request!!)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
    }

    override fun createAndDeliverErrorResult(error: CustomANError) {
        deliverData(WatcherListResponse(error.messageToShow, error.error!!.errorCode))
    }

    override fun isResponseComplete(result: WatcherListResponse): Boolean {
        return true
    }
}