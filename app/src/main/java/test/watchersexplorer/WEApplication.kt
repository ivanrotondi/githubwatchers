package test.watchersexplorer

import com.androidnetworking.AndroidNetworking
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import okhttp3.OkHttpClient
import test.watchersexplorer.di.DaggerAppComponent
import test.watchersexplorer.errors.ErrorManager
import javax.inject.Inject

class WEApplication: DaggerApplication() {

    @Inject
    lateinit var okHttpClient: OkHttpClient

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }

    override fun onCreate() {
        super.onCreate()

        AndroidNetworking.initialize(applicationContext, okHttpClient)
        ErrorManager.appContext = this
    }
}