package test.watchersexplorer.data.parsers

interface IHeaderParser<REQ, RES> {
    fun parse(response: RES, oldRequest: REQ): REQ?
}