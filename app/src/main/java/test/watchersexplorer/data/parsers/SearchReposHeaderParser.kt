package test.watchersexplorer.data.parsers

import test.watchersexplorer.data.models.request.GetSearchRepositoriesRequest
import test.watchersexplorer.data.models.response.BaseResponse
import test.watchersexplorer.data.models.response.SearchRepositoriesResponse

class SearchReposHeaderParser: NextPageParser(), IHeaderParser<GetSearchRepositoriesRequest, SearchRepositoriesResponse> {

    override fun parse(response: SearchRepositoriesResponse, oldRequest: GetSearchRepositoriesRequest): GetSearchRepositoriesRequest? {
        val linkHeader = (response as BaseResponse).linkHeader ?: return null

        val lastQ = oldRequest.param!!
        val newString = linkHeader.replace(lastQ, "")

        if (!checkIfNextExist(newString)) return null

        val list = linkHeader.split(",")

        return GetSearchRepositoriesRequest(lastQ, oldRequest.sort, oldRequest.order, oldRequest.language, getNextPageNumber(list).toInt())
    }
}