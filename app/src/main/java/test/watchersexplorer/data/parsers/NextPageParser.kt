package test.watchersexplorer.data.parsers

import android.net.Uri

abstract class NextPageParser {
    fun getNextPageNumber(list: List<String>?): String {
        if (list == null) return "1"

        for (string in list) {
            if (string.contains("rel=\"next\"")) {
                val lastString = string
                    .replace("<", "")
                    .replace(">", "")
                    .replace(";", "")
                    .replace("rel=\"next\"", "")
                    .replace(" ", "")

                val uri = Uri.parse(lastString)
                return uri.getQueryParameter("page")
            }
        }
        return "1"
    }

    open fun checkIfNextExist(linkHeader: String): Boolean {
        return linkHeader.contains("rel=\"next\"")
    }
}