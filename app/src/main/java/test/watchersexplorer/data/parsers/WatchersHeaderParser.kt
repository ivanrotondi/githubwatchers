package test.watchersexplorer.data.parsers

import test.watchersexplorer.data.models.request.GetListWatcherRequest
import test.watchersexplorer.data.models.response.BaseResponse
import test.watchersexplorer.data.models.response.WatcherListResponse

class WatchersHeaderParser: NextPageParser(), IHeaderParser<GetListWatcherRequest, WatcherListResponse> {

    override fun parse(response: WatcherListResponse, oldRequest: GetListWatcherRequest): GetListWatcherRequest? {
        val linkHeader = (response as BaseResponse).linkHeader ?: return null
        val list = linkHeader.split(",")

        if (!checkIfNextExist(linkHeader)) return null

        return GetListWatcherRequest(oldRequest.userName, oldRequest.repoName, getNextPageNumber(list).toInt())
    }
}