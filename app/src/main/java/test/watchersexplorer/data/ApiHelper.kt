package test.watchersexplorer.data

import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.OkHttpResponseAndParsedRequestListener
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import okhttp3.Response
import test.watchersexplorer.data.models.request.GetListWatcherRequest
import test.watchersexplorer.data.models.request.GetSearchRepositoriesRequest
import test.watchersexplorer.data.models.response.SearchRepositoriesResponse
import test.watchersexplorer.data.models.response.Watcher
import test.watchersexplorer.data.models.response.WatcherListResponse
import java.util.*
import javax.inject.Singleton

@Singleton
class ApiHelper {

    fun getSearchRepository(request: GetSearchRepositoriesRequest): Observable<SearchRepositoriesResponse> {
        val subject = PublishSubject.create<SearchRepositoriesResponse>()

        Rx2AndroidNetworking.get(ApiEndPoint.GET_SEARCH_REPOSITORIES)
            .addQueryParameter(ApiParamsAndPaths.SEARCH_REPOSITORIES_PARAM_PARAM, request.param)
            .addQueryParameter(ApiParamsAndPaths.SEARCH_REPOSITORIES_SORT_PARAM, request.sort)
            .addQueryParameter(ApiParamsAndPaths.SEARCH_REPOSITORIES_ORDER_PARAM, request.order)
            .addQueryParameter(ApiParamsAndPaths.PAGE_LIMIT_PARAM, ApiParamsAndPaths.RATE_LIMIT_REPOS_PER_PAGE.toString())
            .addQueryParameter(ApiParamsAndPaths.PAGE_NUMBER_PARAM, request.pageNumber.toString())
            .build()
            .getAsOkHttpResponseAndObject(SearchRepositoriesResponse::class.java, object: OkHttpResponseAndParsedRequestListener<SearchRepositoriesResponse> {
                override fun onResponse(okHttpResponse: Response?, response: SearchRepositoriesResponse?) {
                    if (response != null) {
                        response.linkHeader = okHttpResponse?.header(ApiParamsAndPaths.LINK_HEADER_PARAM)
                        subject.onNext(response)
                    }
                }

                override fun onError(anError: ANError?) {
                    if (anError != null) {
                        subject.onError(anError)
                    }
                }
            })

        return subject
    }

    fun getWatcherList(request: GetListWatcherRequest): Observable<WatcherListResponse> {
        val subject = PublishSubject.create<WatcherListResponse>()
        val resp = WatcherListResponse()

        Rx2AndroidNetworking.get(ApiEndPoint.GET_WATCHER_LIST)
            .addPathParameter(ApiParamsAndPaths.WATCHER_OWNER_PATH, request.userName)
            .addPathParameter(ApiParamsAndPaths.WATCHER_REPO_PATH, request.repoName)
            .addQueryParameter(ApiParamsAndPaths.PAGE_LIMIT_PARAM, ApiParamsAndPaths.RATE_LIMIT_WATCHERS_PER_PAGE.toString())
            .addQueryParameter(ApiParamsAndPaths.PAGE_NUMBER_PARAM, request.pageNumber.toString())
            .build()
            .getAsOkHttpResponseAndObjectList(Watcher::class.java, object : OkHttpResponseAndParsedRequestListener<List<Watcher>>{
                override fun onResponse(okHttpResponse: Response?, response: List<Watcher>?) {
                    if (response != null) {
                        val linkedList = LinkedList<Watcher>()
                        linkedList.addAll(response)
                        resp.watcherList = linkedList
                        resp.linkHeader = okHttpResponse?.header(ApiParamsAndPaths.LINK_HEADER_PARAM)
                        subject.onNext(resp)
                    }
                }

                override fun onError(anError: ANError?) {
                    if (anError != null) {
                        subject.onError(anError)
                    }
                }

            })

        return subject
    }
}