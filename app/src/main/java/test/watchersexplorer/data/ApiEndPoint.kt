package test.watchersexplorer.data

import test.watchersexplorer.BuildConfig

class ApiEndPoint {
    companion object {
        const val GET_SEARCH_REPOSITORIES: String = BuildConfig.BASE_URL + "search/repositories"

        @JvmStatic
        val GET_WATCHER_LIST: String = BuildConfig.BASE_URL + "repos/{owner}/{repo}/subscribers"
    }
}