package test.watchersexplorer.data

class ApiParamsAndPaths {
    companion object {
        const val SEARCH_REPOSITORIES_PARAM_PARAM: String = "q"

        const val SEARCH_REPOSITORIES_ORDER_PARAM: String = "order"

        const val SEARCH_REPOSITORIES_SORT_PARAM: String = "sort"

        const val SEARCH_REPOSITORIES_LANGUAGE_PARAM: String = "language"

        const val SEARCH_REPOSITORIES_SORT_STARS: String = "stars"

        const val SEARCH_REPOSITORIES_SORT_FORKS: String = "forks"

        const val SEARCH_REPOSITORIES_SORT_UPDATED: String = "updated"

        const val SEARCH_REPOSITORIES_ORDER_DESC: String = "desc"

        const val SEARCH_REPOSITORIES_ORDER_ASC: String = "asc"

        const val WATCHER_OWNER_PATH: String = "owner"

        const val WATCHER_REPO_PATH: String = "repo"

        const val PAGE_LIMIT_PARAM: String = "per_page"

        const val PAGE_NUMBER_PARAM: String = "page"


        const val LINK_HEADER_PARAM: String = "Link"



        const val RATE_LIMIT_REPOS_PER_PAGE: Int = 40

        const val RATE_LIMIT_WATCHERS_PER_PAGE: Int = 33


        const val TIMEOUT_CONNECTION_IN_SECONDS: Long = 10



        const val RATE_LIMIT_EXCEEDED_ERROR_NUMBER: Int = 403

        const val RESULT_1000_REACHED: Int = 422

        const val TIMEOUT_FROM_SERVER_ERROR: Int = 504
    }
}