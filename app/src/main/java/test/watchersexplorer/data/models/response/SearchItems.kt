package test.watchersexplorer.data.models.response

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class SearchItems(
    @Expose
    @SerializedName("id")
    var id: Long,

    @Expose
    @SerializedName("name")
    var name: String? = null,

    @Expose
    @SerializedName("full_name")
    var fullName: String,

    @Expose
    @SerializedName("owner")
    var owner: Owner?,

    @Expose
    @SerializedName("description")
    var description: String? = null,

    @Expose
    @SerializedName("language")
    var language: String? = null,

    @Expose
    @SerializedName("watchers_count")
    var watchersCount: Long = 0,

    @Expose
    @SerializedName("updated_at")
    var updatedAt: String? = null,

    @Expose
    @SerializedName("forks_count")
    var forksCount: Int = 0,

    @Expose
    @SerializedName("stargazers_count")
    var stars: Int = 0) : Parcelable