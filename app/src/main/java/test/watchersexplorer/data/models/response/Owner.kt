package test.watchersexplorer.data.models.response

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Owner(
    @Expose
    @SerializedName("id")
    var id: Long = 0,

    @Expose
    @SerializedName("login")
    var login: String) : Parcelable