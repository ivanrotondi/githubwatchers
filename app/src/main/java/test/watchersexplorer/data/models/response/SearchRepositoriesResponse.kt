package test.watchersexplorer.data.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class SearchRepositoriesResponse(error: String? = null, errorCode: Int = 0): BaseResponse(error, errorCode), PagingResponse<SearchItems> {

    @Expose
    @SerializedName("total_count")
    var totalCount: Long = 0

    @Expose
    @SerializedName("incomplete_results")
    var incompleteResults: Boolean = false

    @Expose
    @SerializedName("items")
    var items: LinkedList<SearchItems>? = null

    override fun addToList(list: LinkedList<SearchItems>) {
        items?.addAll(list)
    }

    override fun getList(): LinkedList<SearchItems>? {
        return items
    }

    override fun getErrorString(): String? {
        return error
    }
}