package test.watchersexplorer.data.models.request

class GetListWatcherRequest(val userName: String, val repoName: String, var pageNumber: Int = 1)