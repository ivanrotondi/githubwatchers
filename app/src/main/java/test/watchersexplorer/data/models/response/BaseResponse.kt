package test.watchersexplorer.data.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class BaseResponse(var error: String? = null,
                        var errorCode: Int = 0) {
    @Expose
    @SerializedName("Response")
    var response: Boolean = false

    var linkHeader: String? = null
}