package test.watchersexplorer.data.models.response

import java.util.*

class WatcherListResponse(error: String? = null, errorCode: Int = 0): BaseResponse(error, errorCode),
    PagingResponse<Watcher> {

    var watcherList: LinkedList<Watcher>? = null

    override fun addToList(list: LinkedList<Watcher>) {
        watcherList?.addAll(list)
    }

    override fun getList(): LinkedList<Watcher>? {
        return watcherList
    }

    override fun getErrorString(): String? {
        return error
    }
}