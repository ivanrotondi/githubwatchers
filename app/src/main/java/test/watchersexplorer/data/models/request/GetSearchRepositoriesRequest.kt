package test.watchersexplorer.data.models.request

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class GetSearchRepositoriesRequest(var param: String? = null, var sort: String = "", var order: String = "", var language: String = "", var pageNumber: Int = 1) : Parcelable