package test.watchersexplorer.data.models.response

import java.util.*

interface PagingResponse<ITEM> {
    fun addToList(list: LinkedList<ITEM>)
    fun getList(): LinkedList<ITEM>?
    fun getErrorString(): String?
}