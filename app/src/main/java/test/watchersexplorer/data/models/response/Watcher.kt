package test.watchersexplorer.data.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Watcher(error: String? = null): BaseResponse() {

    init {
        this.error = error
    }

    @Expose
    @SerializedName("id")
    var id: Long = 0

    @Expose
    @SerializedName("login")
    var login: String? = null

    @Expose
    @SerializedName("avatar_url")
    var avatarUrl: String? = null

    @Expose
    @SerializedName("type")
    var type: String? = null

    @Expose
    @SerializedName("url")
    var url: String? = null
}