package test.watchersexplorer.utlis

import org.junit.Assert
import org.junit.Test

class MethodUtilsTest {

    @Test
    fun testDateParser() {
        val string = MethodUtils.parsingDate("2018-10-15T15:13:48Z")
        Assert.assertEquals(string, "2018-10-15")
    }
}