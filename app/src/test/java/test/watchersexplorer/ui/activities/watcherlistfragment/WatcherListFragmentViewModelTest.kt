package test.watchersexplorer.ui.activities.watcherlistfragment

import lib.baseuiandlogic.utils.rx.LibAppSchedulerProvider
import lib.baseuiandlogic.utils.rx.LibSchedulerProvider
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import test.watchersexplorer.errors.ErrorManager
import test.watchersexplorer.data.ApiHelper
import test.watchersexplorer.data.parsers.WatchersHeaderParser
import test.watchersexplorer.repositories.RepoWatchersRepository
import javax.inject.Inject

class WatcherListFragmentViewModelTest {

    private lateinit var viewModel: WatcherListFragmentViewModel
    private lateinit var repository: RepoWatchersRepository
    private lateinit var apiHelper: ApiHelper
    private lateinit var errorManager: ErrorManager
    private lateinit var schedulerProvider: LibSchedulerProvider

    @Inject
    lateinit var watchersHeaderParser: WatchersHeaderParser


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        apiHelper = Mockito.mock(ApiHelper::class.java)
        errorManager = Mockito.mock(ErrorManager::class.java)
        schedulerProvider = Mockito.mock(LibAppSchedulerProvider::class.java)

        repository = RepoWatchersRepository(apiHelper, schedulerProvider, errorManager, watchersHeaderParser)

        viewModel = WatcherListFragmentViewModel(repository)
    }

    @Test
    fun buildRequest() {
        viewModel.buildRequest("ivan", "githubsearch")

        assert(viewModel.request?.repoName == "githubsearch")
        assert(viewModel.request?.userName == "ivan")
    }
}