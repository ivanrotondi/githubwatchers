package test.watchersexplorer.ui.activities.searchrepofragment

import org.junit.Test

import org.junit.Before
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import test.watchersexplorer.data.models.request.GetSearchRepositoriesRequest
import test.watchersexplorer.repositories.SearchReposRepository

class SearchReposFragmentViewModelTest {

    private lateinit var viewModel: SearchReposFragmentViewModel

    private lateinit var repository: SearchReposRepository

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        repository = Mockito.mock(SearchReposRepository::class.java)
        viewModel = SearchReposFragmentViewModel(repository)
    }

    @Test
    fun isSearchExecuted() {
        buildRequest()
        assert(!viewModel.request?.param.isNullOrEmpty())
    }

    @Test
    fun getSearchedRequest() {
        buildRequest()
        assert(viewModel.getSearchedRequest() != null)
        assert(viewModel.getSearchedRequest() is GetSearchRepositoriesRequest)
    }

    @Test
    fun buildRequest() {
        val map = HashMap<String, String>()
        map["q"] = "tetris"
        map["language"] = "java"

        val sort = "stars"
        val order = "asc"

        viewModel.buildRequest(map, sort, order)

        assert(viewModel.request?.param == "tetris+language:java")
        assert(viewModel.request?.language == "java")
        assert(viewModel.request?.sort == "stars")
        assert(viewModel.request?.order == "asc")
    }

    @Test
    fun buildRequestWithoutSortAndOrder() {
        val map = HashMap<String, String>()
        map["q"] = "tetris"
        map["language"] = "java"


        viewModel.buildRequest(map)

        assert(viewModel.request?.param == "tetris+language:java")
        assert(viewModel.request?.language == "java")
        assert(viewModel.request?.sort == "")
        assert(viewModel.request?.order == "")
    }
}