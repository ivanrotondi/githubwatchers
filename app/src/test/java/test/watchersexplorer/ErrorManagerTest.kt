package test.watchersexplorer

import android.content.Context
import org.junit.Before
import org.junit.Test
import test.watchersexplorer.errors.ErrorManager
import java.util.*

class ErrorManagerTest {

    private val instance = ErrorManager
    private lateinit var mapImpl: ArrayList<String>
    private lateinit var appContext: Context

    @Before
    fun setUp() {
        val map = instance::class.java.getDeclaredField("repoMapError")
        map.isAccessible = true

        mapImpl = map.get(instance) as ArrayList<String>
    }

    @Test
    fun addRepoInExceededList() {
        instance.addRepoInExceededList("WatcherClass")

        assert(mapImpl.contains("WatcherClass"))
    }

    @Test
    fun callExceededEmptyList() {
        mapImpl.clear()

        assert(!instance.isCallExceeded("WatcherClass"))
    }

    @Test
    fun callExceededWithClass() {
        instance.addRepoInExceededList("WatcherClass")

        assert(instance.isCallExceeded("WatcherClass"))
    }

    @Test
    fun addExceededServiceCall() {
        mapImpl.clear()

        instance.addExceededServiceCall(403, "CustomClass")
        assert(mapImpl.contains("CustomClass"))
    }
}