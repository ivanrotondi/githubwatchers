package test.watchersexplorer.data.parsers

import android.net.Uri
import org.junit.Test

import org.junit.Before
import test.watchersexplorer.data.models.request.GetSearchRepositoriesRequest
import test.watchersexplorer.data.models.response.SearchRepositoriesResponse

class SearchReposHeaderParserTest {

    private lateinit var parser: SearchReposHeaderParser

    @Before
    fun setup() {
        parser = SearchReposHeaderParser()
    }

    @Test
    fun parseSuccess() {
        val response = SearchRepositoriesResponse()
        response.linkHeader = "<https://api.github.com/search/repositories?q=android&per_page=40&sort=&page=2&order=>; rel=\"next\", <https://api.github.com/search/repositories?q=android&per_page=40&sort=&page=25&order=>; rel=\"last\""

        val oldRequest = GetSearchRepositoriesRequest()
        oldRequest.pageNumber = 1
        oldRequest.param = "android"

        val newRequest = parser.parse(response, oldRequest)

        assert(newRequest?.pageNumber == ++oldRequest.pageNumber)
        assert(newRequest?.param == oldRequest.param)
    }

    @Test
    fun parseFirstAndLastPaginationRequest() {
        val response = SearchRepositoriesResponse()
        response.linkHeader = null

        val oldRequest = GetSearchRepositoriesRequest()
        oldRequest.pageNumber = 1
        oldRequest.param = "android"

        val newRequest = parser.parse(response, oldRequest)

        assert(newRequest == null)
    }

    @Test
    fun parseLastPaginationRequest() {
        val response = SearchRepositoriesResponse()
        response.linkHeader = "<https://api.github.com/search/repositories?q=android&per_page=40&sort=&page=24&order=>; rel=\"prev\", <https://api.github.com/search/repositories?q=android&per_page=40&sort=&page=25&order=>; rel=\"last\""

        val oldRequest = GetSearchRepositoriesRequest()
        oldRequest.pageNumber = 24
        oldRequest.param = "android"

        val newRequest = parser.parse(response, oldRequest)

        assert(newRequest == null)
    }
}