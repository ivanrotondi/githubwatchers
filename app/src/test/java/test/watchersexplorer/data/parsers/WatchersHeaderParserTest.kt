package test.watchersexplorer.data.parsers

import org.junit.Before
import org.junit.Test
import test.watchersexplorer.data.models.request.GetListWatcherRequest
import test.watchersexplorer.data.models.response.WatcherListResponse
import java.util.*

class WatchersHeaderParserTest {

    private lateinit var parser: WatchersHeaderParser

    @Before
    fun setup() {
        parser = WatchersHeaderParser()
    }

    @Test
    fun parse() {
        val linkHeader = "<https://api.github.com/repositories/12544093/subscribers?per_page=33&page=2>; rel=\"next\", <https://api.github.com/repositories/12544093/subscribers?per_page=33&page=14>; rel=\"last\""

        val response = WatcherListResponse()
        response.watcherList = LinkedList()
        response.linkHeader = linkHeader

        val oldRequest = GetListWatcherRequest("user", "repo")
        oldRequest.pageNumber = 1

        assert(parser.parse(response, oldRequest)?.pageNumber == ++oldRequest.pageNumber)
    }
}