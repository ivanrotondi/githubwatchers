package test.watchersexplorer.repositories

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.androidnetworking.error.ANError
import io.reactivex.Scheduler
import io.reactivex.schedulers.TestScheduler
import io.reactivex.subjects.PublishSubject
import lib.baseuiandlogic.utils.livedata.LibSingleLiveEvent
import lib.baseuiandlogic.utils.rx.LibSchedulerProvider
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations
import test.watchersexplorer.data.ApiHelper
import test.watchersexplorer.data.models.request.GetSearchRepositoriesRequest
import test.watchersexplorer.data.models.response.SearchItems
import test.watchersexplorer.data.models.response.SearchRepositoriesResponse
import test.watchersexplorer.data.parsers.SearchReposHeaderParser
import test.watchersexplorer.di.ParserForReposSearch
import test.watchersexplorer.errors.ErrorManager
import java.util.*
import java.util.regex.Pattern

class SearchReposRepositoryTest {

    @InjectMocks
    lateinit var apiHelper: ApiHelper

    private val schedulerProvider: LibSchedulerProvider = object : LibSchedulerProvider {
        override fun computation(): Scheduler {
            return TestScheduler()
        }

        override fun io(): Scheduler {
            return TestScheduler()
        }

        override fun ui(): Scheduler {
            return TestScheduler()
        }

    }

    @InjectMocks
    lateinit var errorManagerTest: ErrorManager

    @InjectMocks
    @ParserForReposSearch
    lateinit var parser: SearchReposHeaderParser

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    lateinit var repo: SearchReposRepository


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)

        repo = SearchReposRepository(apiHelper, schedulerProvider, errorManagerTest, parser)

        val showLoaderLiveData = LibSingleLiveEvent<Boolean>()
        repo.showLoaderLiveData = showLoaderLiveData
    }

    @Test
    fun fillObservable() {
        repo.request = GetSearchRepositoriesRequest("q=java")

        repo.fillObservable()
    }

    @Test
    fun startPagination() {
        val request = GetSearchRepositoriesRequest()
        request.param = "android+language:java"

        repo.startPagination(request)

        assert(repo.request?.param == "android+language:java")
    }

    @Test
    fun callInExecution() {
        repo.callRunning = false

        repo.callInExecution()

        assert(repo.callRunning)
    }

    @Test
    fun cancelCallAndInitRepository() {
        repo.observableDisposable = PublishSubject.create<SearchRepositoriesResponse>().subscribe()

        repo.response = SearchRepositoriesResponse()

        repo.cancelCallAndInitRepository()

        assert(repo.response == null)
        assert(repo.observableDisposable!!.isDisposed)
    }

    @Test
    fun handleSuccessResponseIncomplete() {
        val response = SearchRepositoriesResponse()
        response.incompleteResults = true

        assert(!repo.isResponseComplete(response))
    }

    @Test
    fun handleSuccessResponseFillResponse() {
        val response = SearchRepositoriesResponse()
        response.items = LinkedList<SearchItems>()
        response.incompleteResults = false

        val request = GetSearchRepositoriesRequest()
        request.param = "android"

        repo.request = request
        repo.handleSuccessResponse(response)

        assert(repo.response == response)
    }
}